
import com.lihua.evaluator.Evaluator;
import com.lihua.evaluator.InvalidInputException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


/**
 * This test class uses three samples to test if input valid chars
 * InvalidInputException thrown
 * @author Lihua
 */
@RunWith(Parameterized.class)
public class TestInvalidInput {
     Queue<String> inputQue;
    private Evaluator evaluator;

    @Before
    public void initialize() {
        this.evaluator = new Evaluator(inputQue);
    }

    public TestInvalidInput(Queue<String> inputQue) {
        this.inputQue = inputQue;

    }
    @Parameters
    public static Collection addQues() {

        return Arrays.asList(new Object[][]{
            {new ArrayDeque<>(Arrays.asList("1", "+", "a"))},
            {new ArrayDeque<>(Arrays.asList("^","9"))},
            {new ArrayDeque<>(Arrays.asList("{","+","9"))}
        });
    }
                
                
    
    @Test(expected= InvalidInputException.class)
    public void testEvaluator() throws Exception {
        Evaluator e = new Evaluator(inputQue);
        boolean result = e.checkIfInputValid(inputQue);
        assertEquals("The input is valid exception",false, result );
        
    }
    
}

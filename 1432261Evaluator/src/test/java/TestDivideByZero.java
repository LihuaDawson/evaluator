
import com.lihua.evaluator.BinaryExpressionException;
import com.lihua.evaluator.DivideByZeroException;
import com.lihua.evaluator.Evaluator;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


/**
 * This test class uses three sample to test if divideByZeroException thrown
 * when user try to divide a number by zero
 * @author Lihua
 */
@RunWith(Parameterized.class)
public class TestDivideByZero {
    Queue<String> inputQue;
    private Evaluator evaluator;

    @Before
    public void initialize() {
        this.evaluator = new Evaluator(inputQue);
    }

    public TestDivideByZero(Queue<String> inputQue) {
        this.inputQue = inputQue;

    }
    @Parameterized.Parameters
    public static Collection addQues() {

        return Arrays.asList(new Object[][]{
            {new ArrayDeque<>(Arrays.asList("1", "+", "3","/","0"))},
            {new ArrayDeque<>(Arrays.asList("3","/","0","*","2"))},
            {new ArrayDeque<>(Arrays.asList("1","+","2","/","7","/","0"))}
        });
    }
                
                
    
    @Test(expected= DivideByZeroException.class)
    public void testEvaluator() throws Exception {
        Evaluator e = new Evaluator(inputQue);
        e.evaluate();
        
        
    }
}

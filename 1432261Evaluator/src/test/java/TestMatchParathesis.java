
import com.lihua.evaluator.Evaluator;
import com.lihua.evaluator.Non_Matching_ParenthesisException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;



/**
 * This test class uses three samples to test when parenthesis are not match
 * the Non_Matching_ParenthesisException should be thrown
 * @author Lihua
 */
@RunWith(Parameterized.class)
public class TestMatchParathesis {
      Queue<String> inputQue;
    private Evaluator evaluator;

    @Before
    public void initialize() {
        this.evaluator = new Evaluator(inputQue);
    }

    public TestMatchParathesis(Queue<String> inputQue) {
        this.inputQue = inputQue;

    }
    @Parameterized.Parameters
    public static Collection addQues() {

        return Arrays.asList(new Object[][]{
            {new ArrayDeque<>(Arrays.asList("1", "+", "(","3","+","4"))},
            {new ArrayDeque<>(Arrays.asList("3","*","(","2","+","3"))},
            {new ArrayDeque<>(Arrays.asList("1","+","2",")","*","9"))}
        });
    }
                
                
    
    @Test(expected= Non_Matching_ParenthesisException.class)  
    public void testEvaluator() throws Exception {
        Evaluator e = new Evaluator(inputQue);
        boolean result = e.checkIfInputValid(inputQue);
        assertEquals("The parathensis are matching",false, result );
        
    }
    
}

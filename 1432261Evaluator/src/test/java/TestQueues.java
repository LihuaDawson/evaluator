
import com.lihua.evaluator.Evaluator;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


/**
 * This test class uses 25 samples queue to test the evaluators works properly
 * @author Lihua 
 */
@RunWith(Parameterized.class)
public class TestQueues {

    Queue<String> inputQue;
    private Evaluator evaluator;
    double expected;

    @Before
    public void initialize() {
        evaluator = new Evaluator(inputQue);
    }

    public TestQueues(Queue<String> inputQue, double expected) {
        this.inputQue = inputQue;
        this.expected = expected;
    }

    @Parameters
    public static Collection addQues() {

        return Arrays.asList(new Object[][]{
            {new ArrayDeque<>(Arrays.asList("1", "+", "2")), 3.0},
            {new ArrayDeque<>(Arrays.asList("2", "*", "2.5")), 5.0},
            {new ArrayDeque<>(Arrays.asList("1", "+", "2", "*", "2.5")), 6.0},
            {new ArrayDeque<>(Arrays.asList("2", "*", "(", "1", "+", "2", ")")), 6.0},
            {new ArrayDeque<>(Arrays.asList("2", "(", "8", "+", "2", ")", "(", "1", "+", "3", ")", "+", "5")), 85.0},
            {new ArrayDeque<>(Arrays.asList("3", "*", "6", "/", "3")), 6.0},
            {new ArrayDeque<>(Arrays.asList("1", "+", "3", "*", "6", "/", "3")), 7.0},
            {new ArrayDeque<>(Arrays.asList("1", "+", "3", "*", "6", "/", "3", "-", "1")), 6.0},
            {new ArrayDeque<>(Arrays.asList("8", "-", "3", "*", "6", "-", "1")), -11.0},
            {new ArrayDeque<>(Arrays.asList("9", "(", "8", "-", "2", "*", "3", ")", "/", "5", "*", "2")), 7.2},
            {new ArrayDeque<>(Arrays.asList("2", "(", "10", "-", "2", "*", "4", ")", "/", "1", "*", "2")), 8.0},
            {new ArrayDeque<>(Arrays.asList("(", "10", "-", "2", "*", "4", ")", "/", "4", "-", "2")), -1.5},
            {new ArrayDeque<>(Arrays.asList("2", "(", "8", "+", "2", ")", "(", "1", "+", "3", ")", "/", "5")), 16.0},
            {new ArrayDeque<>(Arrays.asList("2", "/", "(", "1", "-", "2", ")")), -2.0},
            {new ArrayDeque<>(Arrays.asList("1", "-", "3", "*", "6", "*", "3", "+", "19")), -34.0},
            {new ArrayDeque<>(Arrays.asList("(", "10", "-", "2", "*", "4", ")", "4", "-", "2")), 6.0},
            {new ArrayDeque<>(Arrays.asList("(", "10", "-", "2", "*", "4", ")", "-4", "-", "2")), -10.0},
            {new ArrayDeque<>(Arrays.asList("(", "1", "-", "2", "*", "4", ")", "-4", "-", "2")), 26.0},
            {new ArrayDeque<>(Arrays.asList("0", "(", "8", "+", "2", ")", "(", "1", "+", "3", ")", "+", "5")), 5.0},
            {new ArrayDeque<>(Arrays.asList("(", "8", "+", "2", ")", "/", "(", "1", "+", "1", ")", "+", "5")), 10.0},
            {new ArrayDeque<>(Arrays.asList("(", "8", "+", "2", ")", "/", "(", "1", "+", "1", ")", "/", "5")), 1.0},
            {new ArrayDeque<>(Arrays.asList("(", "8", "+", "2", ")", "-", "(", "1", "+", "4", ")", "/", "5")), 9.0},
            {new ArrayDeque<>(Arrays.asList("2", "*", "(", "1", "-", "2", ")")), -2.0},
            {new ArrayDeque<>(Arrays.asList("2", "+", "(", "1", "-", "2", ")", "/", "(", "1", "-", "5", ")")), 2.25},
            {new ArrayDeque<>(Arrays.asList("2", "+", "(", "1", "-", "2", ")", "/", "1", "-", "5")), -4.0},
            {new ArrayDeque<>(Arrays.asList("2.5", "+", "(", "1.2", "-", "2", ")", "*", "1", "-", "5.6")), -3.9},});

    }

    @Test
    public void testEvaluator() throws Exception {
        Evaluator e = new Evaluator(inputQue);
        
        assertEquals(expected, Double.parseDouble(e.evaluate()), 0.0000000001);
    }

}

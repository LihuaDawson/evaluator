
import com.lihua.evaluator.BinaryExpressionException;
import com.lihua.evaluator.Evaluator;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


/**
 * This test class has three samples to test if BinaryExpressionException
 * thrown when binary expression happens
 * @author Lihua
 */
@RunWith(Parameterized.class)
public class TestBinaryExpression {
    Queue<String> inputQue;
    private Evaluator evaluator;

    @Before
    public void initialize() {
        this.evaluator = new Evaluator(inputQue);
    }

    public TestBinaryExpression(Queue<String> inputQue) {
        this.inputQue = inputQue;

    }
    @Parameterized.Parameters
    public static Collection addQues() {

        return Arrays.asList(new Object[][]{
            {new ArrayDeque<>(Arrays.asList("1", "+", "*","3","+","4"))},
            {new ArrayDeque<>(Arrays.asList("3","*","-","2","+","3"))},
            {new ArrayDeque<>(Arrays.asList("1","+","2","*","*","9"))}
        });
    }
                
                
    
    @Test (expected = BinaryExpressionException.class)
    public void testEvaluator() throws Exception {
        Evaluator e = new Evaluator(inputQue);
        e.evaluate();
 
    }
    
}

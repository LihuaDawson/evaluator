
package com.lihua.evaluator;

/**
 * This exception check if there has invalid input chars
 * @author Lihua
 */
public class InvalidInputException extends Exception{
    public InvalidInputException(String message) {
        super(message);

    }
    
}

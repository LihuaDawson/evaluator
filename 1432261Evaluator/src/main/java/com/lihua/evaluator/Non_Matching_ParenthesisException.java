
package com.lihua.evaluator;

/**
 * This exception is for parenthesis not matching in one queue
 * @author Lihua
 */
public class Non_Matching_ParenthesisException extends Exception{
        public Non_Matching_ParenthesisException(String message) {
        super(message);
    }
    
}

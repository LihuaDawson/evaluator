
package com.lihua.evaluator;

/**
 * This exception happens when two operators or two operands
 * beside each other
 * @author Lihua
 */
public class BinaryExpressionException extends Exception{
       public BinaryExpressionException(String message) {
        super(message);
    }
 
}

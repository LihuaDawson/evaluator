
package com.lihua.evaluator;

/**
 * This exception happens when user try to divide a number by zero
 * @author Lihua
 */
public class DivideByZeroException extends Exception{
    
    public DivideByZeroException(String message){
        super(message);
    }
    
}

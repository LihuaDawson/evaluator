
package com.lihua.evaluator;

/**
 * This constants are created for exception messages
 * @author Lihua
 */
public class Constant {
    
    public static final String INVALID_INPUT_MESSAGE = "You have invalid char in input queue!";
    public static final String BINARY_EXPRESSION_MESSAGE = "You have binary expression in your input queue!";
    public static final String NON_MATCHING_PARENTHESIS_MESSAGE="the parenthesis number in your input is not matching!";
    public static final String DIVIDE_BY_ZEROEXCEPTION = "You can not divide by zero!";
}


package com.lihua.evaluator;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.Queue;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * This class receive a queue of strings,
 * first validate the queue is valid input
 * second convert the queue from infix to postfix
 * finally calculate the queue
 * @author Lihua
 */
public class Evaluator {

    private final static Logger LOG = LoggerFactory.getLogger(Evaluator.class);
    Queue<String> inputQue;
    Deque<String> operatorsStack;
    Queue<String> postFixQue;
    Queue<String> modifiedInputQue;

    public Evaluator(Queue inputQue) {
        this.operatorsStack = new ArrayDeque<>();
        this.inputQue = new ArrayDeque<>();
        this.inputQue = inputQue;
        this.postFixQue = new ArrayDeque<>();
        this.modifiedInputQue = new ArrayDeque<>();
    }

    /**
     * This method calculate the modified queue
     * @return
     * @throws Exception 
     */
    public String evaluate() throws Exception {
        checkIfInputValid(inputQue);
        infixToPostfix();
        Iterator<String> s = postFixQue.iterator();
        String current = "";
        while (s.hasNext()) {
            current = s.next();
            if (isOperand(current)) {

                operatorsStack.add(current);
                LOG.debug(current+"->operand is added");
            }
            if (isOperator(current)) {
                
                String str2 = operatorsStack.removeLast();
                LOG.debug(str2+"->removed from operators stack");
                double num2 = Double.parseDouble(str2);
                String str1 = operatorsStack.removeLast();
                LOG.debug(str1+"->removed from operators stack");
                double num1 = Double.parseDouble(str1);

                String answer = calculate(num1, num2, current);
                LOG.debug("answer is :"+answer);
                operatorsStack.add(answer);
            }
        }
        LOG.debug("answer is added to operator stack :"+operatorsStack.element());
        return operatorsStack.element();
    }

    /**
     * This method calculate two given operands and one operator
     * @param num1 double operand
     * @param num2 double operand
     * @param current string operator
     * @return string as an answer
     * @throws Exception 
     */
    private String calculate(double num1, double num2, String current) throws Exception {

        
        double result;
        switch (current) {
            case "+":
                result = num1 + num2;
                break;
            case "-":
                result = num1 - num2;
                break;
            case "*":
                result = num1 * num2;
                break;
            case "/":
                if (num2 == 0) {
                    LOG.info("divid by zero");
                    throw new DivideByZeroException(Constant.DIVIDE_BY_ZEROEXCEPTION);
                } else {
                    result = num1 / num2;
                    break;
                }
            default:
                throw new Exception();
        }
        LOG.debug("the result of caculate is: "+result);
        return String.valueOf(result);
    }

    /**
     * This method accept one input queue which eliminate all spaces and checked invalid inputs
     * then check if inside the queue exist binary expression, if not continue to convert the queue 
     * from infix to postfix and return new queue
     * @return queue of string which is a postfix queue
     * @throws InvalidInputException
     * @throws BinaryExpressionException
     * @throws Non_Matching_ParenthesisException
     * @throws DivideByZeroException 
     */
    public Queue<String> infixToPostfix() throws InvalidInputException, BinaryExpressionException, Non_Matching_ParenthesisException, DivideByZeroException {
        String currentStr = "";
        String previousStr = "";
       
        checkBinaryExp(modifiedInputQue);
        Iterator<String> s = modifiedInputQue.iterator();
        int countP = 0;
        while (s.hasNext()) {
            currentStr = s.next();
            
            if (isOperand(currentStr)) {
                 postFixQue.offer(currentStr);
                LOG.debug(currentStr+"-> add to postfixque");
        

            }
            if (currentStr.equals("(")) {
                countP++;
                operatorsStack.add(currentStr);
                LOG.debug("count parenthesis: "+countP);
                LOG.debug("operator stack added: "+currentStr);
            }
            if (currentStr.equals(")")) {
                
                while (!operatorsStack.peekLast().equals("(") && countP > 0) {
                    String next = operatorsStack.removeLast();
                    LOG.debug(next+"->removed from operator stack");
                    postFixQue.add(next);
                    LOG.debug(next+"->added to postfixque");
                }
                if (operatorsStack.peekLast().equals("(")) {
                    countP--;
                    String delete = operatorsStack.removeLast();
                    LOG.debug(delete+"-> is deleted from operator stack");
                }
            }
            String top = operatorsStack.peekLast();
            
            if (isOperator(currentStr)) {
                

                if (operatorsStack.isEmpty()) {
                    operatorsStack.push(currentStr);

                } 
                else {
                    
                    if (isOperator(top)) {

                        if (priority(top) >=priority(currentStr)) {
                            String pop = operatorsStack.removeLast();

                            postFixQue.add(pop);
                            LOG.debug(top+"-> is added to postfixque");
                            if(operatorsStack.peekLast()!=null && isOperator(operatorsStack.peekLast()))
                            {
                                
                                if(priority(operatorsStack.peekLast())>=priority(currentStr))
                                {
           
                                    postFixQue.add(operatorsStack.removeLast());
                                    operatorsStack.add(currentStr);
                                }
                                else
                                {
                                    operatorsStack.add(currentStr);
                                }
                            }
                            else
                            {
                                operatorsStack.add(currentStr);
                            }
                        }
                        
                        else {
    
                               operatorsStack.add(currentStr);
                      
                        }
                    }
                    if (top.equals("(")) {
                        operatorsStack.add(currentStr);
                    }
                }
            }
         
        }
        while (!operatorsStack.isEmpty()) {
            String str = operatorsStack.removeLast();
            postFixQue.offer(str);
        }

        return postFixQue;
    }

    /**
     * This method set the priority of operation
     * @param currentStr
     * @return 
     */
    private int priority(String currentStr) {
        switch (currentStr) {
            case "+":
                return 1;
            case "-":
                return 1;
            case "*":
                return 2;
            case "/":
                return 2;
            default:
                return 0;
        }
    }

    /**
     * This method determine if a string is a parenthesis or not
     * @param currentStr
     * @return Boolean
     */
    private boolean isParenthesis(String currentStr) {
        return (currentStr.length() == 1 && currentStr.matches("[()]"));
    }

    /**
     * This method distinguish if the currentStr is an operator or not
     * @param currentStr
     * @return Boolean
     */
    private boolean isOperator(String currentStr) {

        return (currentStr.length() == 1 && currentStr.matches("[+-//*/]"));

    }

    /**
     * This method check if the input queue is valid input characters and eliminate all spaces inside
     * @param inputQue
     * @return Boolean
     * @throws InvalidInputException
     * @throws BinaryExpressionException
     * @throws Non_Matching_ParenthesisException
     * @throws DivideByZeroException 
     */
    public boolean checkIfInputValid(Queue<String> inputQue) throws InvalidInputException, BinaryExpressionException, Non_Matching_ParenthesisException, DivideByZeroException {

        boolean isValidChar = true;
        boolean validParenthensis = true;
        String previousStr = "";
        String currentStr = "";
        int startPCount = 0;
        int endPCount = 0;

        Iterator<String> s = inputQue.iterator();
        while (s.hasNext()) {
            currentStr = s.next().replace(" ", "");

            isValidChar = checkValidChar(currentStr);

            isNeedMultiplication(previousStr, currentStr);
            if (currentStr.equals("(")) {
                startPCount++;

            }
            if (currentStr.equals(")")) {
                endPCount++;

            }

            modifiedInputQue.offer(currentStr);
            previousStr = currentStr;

        }
    
        if (startPCount != endPCount) {
            validParenthensis = false;
            throw new Non_Matching_ParenthesisException(Constant.NON_MATCHING_PARENTHESIS_MESSAGE);
        }

        return isValidChar && validParenthensis;

    }

    /**
     * This method determine if the input queue need to add multiplications 
     * @param previousStr
     * @param currentStr 
     */
    private void isNeedMultiplication(String previousStr, String currentStr) {
        if (currentStr.equals("(")) {

            if (!isOperator(previousStr) && !previousStr.isEmpty()) {
                modifiedInputQue.offer("*");
            }
        }
        if (previousStr.equals(")") && isOperand(currentStr)) {
            modifiedInputQue.offer("*");
        }

    }

    /**
     * This method check if input string is valid char
     * it check if it's empty string or the chars not used in math calculations
     * or if the string contains more than 1 dot
     * @param currentStr
     * @return
     * @throws InvalidInputException 
     */
    private boolean checkValidChar(String currentStr) throws InvalidInputException {
        boolean isValidChar = true;
        if (currentStr.length() == 0) {
            isValidChar = false;
            throw new InvalidInputException(Constant.INVALID_INPUT_MESSAGE);
        }

        if (!isOperand(currentStr) && !isOperator(currentStr) && !isParenthesis(currentStr)) {
            isValidChar = false;
            throw new InvalidInputException(Constant.INVALID_INPUT_MESSAGE);

        }
        if (isMoreThanTwoDots(currentStr)) {
            isValidChar = false;
            throw new InvalidInputException(Constant.INVALID_INPUT_MESSAGE);

        }

        return isValidChar;
    }

    private boolean isMoreThanTwoDots(String currentStr) {
        int count = 0;
        for (char c : currentStr.toCharArray()) {
            if (c == '.') {
                count++;
            }
        }
        return count > 1;
    }

    /**
     * This method used to check if the queue contains two operators together or
     * two operands together
     * @param modifiedInputQue
     * @return Boolean
     * @throws BinaryExpressionException 
     */
    private boolean checkBinaryExp(Queue<String> modifiedInputQue) throws BinaryExpressionException {
        boolean isNotBinary = true;
        String current;
        String previous = "";
        Iterator i = modifiedInputQue.iterator();
        while (i.hasNext()) {
            current = (String) i.next();
            if (!previous.isEmpty()) {
                if (isOperator(current) && isOperator(previous)) {
                    isNotBinary = false;
                    LOG.info("binary expression");
                    throw new BinaryExpressionException(Constant.BINARY_EXPRESSION_MESSAGE);
                }
            }
            previous = current;
        }

        return isNotBinary;
    }

    /**
     * This method used to check if the input string is an operand or not
     * @param currentStr
     * @return Boolean
     */
    private boolean isOperand(String currentStr) {
        char firstChar = currentStr.charAt(0);
        if (currentStr.length() > 1) {

            String restStr = currentStr.substring(1);
            boolean firstCharIsNum = (firstChar <= '9' && firstChar >= '0');
            boolean firstCharIsSign = (firstChar == '+' || firstChar == '-');
            boolean firstCharValid = (firstCharIsNum || firstCharIsSign);
            boolean restAllNum = restStr.matches("[0-9.]*");

            return (firstCharValid && restAllNum);
        } else {
            return (firstChar <= '9' && firstChar >= '0');
        }

    }
}
